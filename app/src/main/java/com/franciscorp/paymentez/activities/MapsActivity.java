package com.franciscorp.paymentez.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.dtos.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import static com.franciscorp.paymentez.activities.MainActivity.KEY_PLACES;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Place> places;
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        places = getIntent().getParcelableArrayListExtra(KEY_PLACES);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a markers and move the camera
        paintMarkers();
    }
    public void paintMarkers() {
        if (mMap != null) {
            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    LatLng pointLatLong;
                    for (int i = 0; i < places.size(); i++) {
                        pointLatLong = places.get(i).getLatLng();
                        builder.include(pointLatLong);

                        Marker marker = mMap.addMarker(new MarkerOptions().position(pointLatLong).title(places.get(i).getAddress()).anchor(0.5f, 0.5f));
                    }
                    moveCameraBounds();
                }
            });
        }
    }

    private void moveCameraBounds() {
        LatLngBounds bounds = builder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 150);
        mMap.animateCamera(cameraUpdate);
    }
}

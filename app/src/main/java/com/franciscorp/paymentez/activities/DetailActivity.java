package com.franciscorp.paymentez.activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.transition.Explode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.adapters.AdapterListProducts;
import com.franciscorp.paymentez.dtos.Place;
import com.franciscorp.paymentez.dtos.Product;
import com.franciscorp.paymentez.util.PermissionUtils;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    public static final String KEY_PLACE = "placeKey";
    public static final String KEY_IMAGE = "imageKey";
    private Intent intent;
    private Place place;
    private TextView title;
    private ImageView image;
    private ShareActionProvider mShareActionProvider;
    private Intent shareIntent;
    private final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 626;
    private Bitmap bmp;
    private ListView listProducts;
    private ArrayList<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        title = findViewById(R.id.title);
        image = findViewById(R.id.image);
        listProducts = findViewById(R.id.list_products);

        intent = getIntent();
        place = intent.getParcelableExtra(KEY_PLACE);

        createShareInetnt();
        getImage();

        image.setImageBitmap(bmp);
        title.setText(place.getName() + " " + place.getAddress() + "(GoogleMaps)");
        title.setPaintFlags(title.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        setupWindowAnimations();

        setListProducts();
    }

    private void setListProducts() {

        addProduct("https://www.pepsi.com/en-us/uploads/images/can-pepsi.png", "Pepsi", "$30");
        addProduct("https://inlivo-vrabm4g9pc6mip71kb.stackpathdns.com/viktor_ftp/WEB/inlivo_new/prod_pics/thumb/13.jpg", "Ensalada", "$100");
        addProduct("https://images.vexels.com/media/users/3/147027/isolated/lists/ed20fb7d61c5a3e634a4c1c430c56753-rebanada-de-pastel-con-fresa.png", "Pastel", "$50");
        addProduct("https://i.pinimg.com/originals/f0/79/08/f079083db303ee57f93a45ee4623e9ee.jpg", "Helado", "$50");
        addProduct("http://icdn.pro/images/es/d/e/de-alimentos-taza-de-cafe-icono-7718-128.png", "Cafe", "$60");
        addProduct("https://pbs.twimg.com/profile_images/3033155153/499c6bc225e03320c1eb44a537a64c73.png", "Carne", "$150");
        addProduct("https://blog.blueapron.com/wp-content/uploads/2013/09/Pasta-Tricks-from-Blue-Apron-256x256.jpg", "Pasta", "$100");

        AdapterListProducts adapterListProducts = new AdapterListProducts(this, products);
        listProducts.setAdapter(adapterListProducts);
    }

    private void addProduct(String urlImge, String name, String price) {
        Product product = new Product(urlImge,name,price);
        products.add(product);
    }


    private void setupWindowAnimations() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Explode enterTransition = new Explode();
            enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
            getWindow().setExitTransition(enterTransition);
        }
    }
    private void createShareInetnt() {
        shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        String link=" https://www.google.com/maps/search/?api=1&query="+place.getLatLng().latitude+","+place.getLatLng().longitude+"&query_place_id="+place.getId();

        shareIntent.putExtra(Intent.EXTRA_TEXT, place.getName() + " " + place.getAddress()+" _ "+ link);
    }

    private void getImage() {

        if (intent.hasExtra(KEY_IMAGE)) {
            byte[] byteArray = getIntent().getByteArrayExtra(KEY_IMAGE);
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            setImageShareIntent(bmp);
        } else {
            bmp = BitmapFactory.decodeResource(getResources(), R.drawable.default_image);
            shareIntent.setType("text/plain");
        }
        setShareIntent();
    }

    public void setImageShareIntent(Bitmap inImage) {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", null);
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
            shareIntent.setType("image/*");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_detail, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setShareIntent();
        // Return true to display menu
        return true;
    }

    // Call to update the share intent
    private void setShareIntent() {
        if (mShareActionProvider != null && shareIntent != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    private boolean checkPermission(String permission) {

        if (ContextCompat.checkSelfPermission(this,
                permission) != PackageManager.PERMISSION_GRANTED) {

            PermissionUtils.requestPermission(this, PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE,
                    permission, true);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setImageShareIntent(bmp);
                    setShareIntent();
                }
                return;
            }
        }
    }

    public void onClickAddress(View view) {
        Uri gmmIntentUri = Uri.parse("https://www.google.com/maps/search/?api=1&query="+place.getLatLng().latitude+","+place.getLatLng().longitude+"&query_place_id="+place.getId());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    public void btnWaze(View view) {
        try {

            if (place.getLatLng() != null) {
                String url = "waze://?ll=" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&z=10&navigate=yes";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        } catch (ActivityNotFoundException ex) {
            Intent intent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
        }
    }
}

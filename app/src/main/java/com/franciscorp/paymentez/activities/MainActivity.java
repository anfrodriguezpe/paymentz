package com.franciscorp.paymentez.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.adapters.AdapterRecyclerPlaces;
import com.franciscorp.paymentez.dtos.Place;
import com.franciscorp.paymentez.managers.ManagerLocation;
import com.franciscorp.paymentez.managers.ManagerProgressDialog;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final String KEY_PLACES = "placesKey";
    private RecyclerView mRecyclerViewPlaces;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    private PlaceDetectionClient mPlaceDetectionClient;

    private ManagerLocation managerLocation;
    private ManagerProgressDialog progressDialog;
    private int spanCount = 2;
    private ArrayList<Place> places = new ArrayList<Place>();
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerViewPlaces = (RecyclerView) findViewById(R.id.recycler_places);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this);

        // Construct a ManagerLocation.
        managerLocation = new ManagerLocation(this);
        progressDialog = new ManagerProgressDialog(this);

        if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE) {
            spanCount = 3;
        } else {
            spanCount = 2;
        }
        if (savedInstanceState == null) {
            getPlaces();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putParcelableArrayList(KEY_PLACES, places);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        places = savedInstanceState.getParcelableArrayList(KEY_PLACES);
        settingRecycler();
    }

    private void getPlaces() {
        if (managerLocation.isAllowed()) {
            progressDialog.showProgress();
            Task<PlaceLikelihoodBufferResponse> placeResult = mPlaceDetectionClient.getCurrentPlace(null);
            placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                    progressDialog.dismissProgress();
                    try {
                        PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                        Place place;
                        com.google.android.gms.location.places.Place placeGoogle;

                        for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                            placeGoogle = placeLikelihood.getPlace();
                            place = new Place(placeGoogle.getId(), placeGoogle.getPlaceTypes(), placeGoogle.getAddress(), placeGoogle.getLocale(), placeGoogle.getName(),
                                    placeGoogle.getLatLng(), placeGoogle.getViewport(), placeGoogle.getWebsiteUri(), placeGoogle.getPhoneNumber(), placeGoogle.getRating(),
                                    placeGoogle.getPriceLevel(), placeGoogle.getAttributions());
                            places.add(place);
                            Log.i(TAG, String.format("Place '%s' has likelihood: %g",
                                    placeLikelihood.getPlace().getName(),
                                    placeLikelihood.getLikelihood()));
                        }

                        settingRecycler();

                        likelyPlaces.release();
                    } catch (Exception e) {
                        if (e != null) {
                            if (e.getMessage() != null)
                                Log.e(TAG, e.getMessage());
                        }
                        finish();
                    }
                }
            });
        }
    }

    public void search(String searchText){
        if (!TextUtils.isEmpty(searchText)) {
            ArrayList<Place> listFound = new ArrayList<>();
            for (Place item : places) {
                if (item.getName().toLowerCase().contains(searchText.toLowerCase()) || item.getAddress().contains(searchText) ) {
                    listFound.add(item);
                }
            }

            setArrayListToRecycler(listFound);
        } else {

            setArrayListToRecycler(places);
        }
    }
    private void setArrayListToRecycler(ArrayList<Place> placeArrayList){
        mAdapter = new AdapterRecyclerPlaces(placeArrayList, this);
        mRecyclerViewPlaces.setAdapter(mAdapter);
    }

    private void settingRecycler() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewPlaces.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(this, spanCount);
        mRecyclerViewPlaces.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        mAdapter = new AdapterRecyclerPlaces(places, this);
        mRecyclerViewPlaces.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Get the SearchView

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        settingSearchView();
        return true;
    }

    //set the searchable configuration
    private void settingSearchView() {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.maps:
                intent = new Intent(this, MapsActivity.class);
                intent.putParcelableArrayListExtra(KEY_PLACES, places);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.franciscorp.paymentez.managers;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.util.PermissionUtils;

public class ManagerLocation{

    private Context context;
    private LocationManager locationManager;
    private boolean showTurnOnGPS = true;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 123;

    public ManagerLocation(Context context) {

        this.context =context;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        checkLocation();
    }

    public boolean checkLocation() {
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            turnGPSOn();
        }
        return isGPSEnabled;
    }

    public static boolean checkLocation(Context context) {
        LocationManager locationManager;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            turnGPSOn(context);
        }
        return isGPSEnabled;
    }

    public static synchronized void turnGPSOn(final Context mContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.ThemeDialogCustom);
        builder.setMessage(mContext.getString(R.string.gps_not_enable))
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                }).setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).create().show();
    }

    public synchronized void turnGPSOn() {

        if (showTurnOnGPS) {
            showTurnOnGPS = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.ThemeDialogCustom);
            builder.setMessage(context.getString(R.string.gps_not_enable))
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            showTurnOnGPS = true;
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(intent);
                        }
                    }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showTurnOnGPS = true;
                    Toast.makeText(context,
                            R.string.gps_required_toast,
                            Toast.LENGTH_SHORT)
                            .show();

                }
            }).create().show();
        }
    }

    public synchronized Boolean isAllowed() {

        try {
            boolean permissionfineLocation= (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            boolean permissionCoarseLocation= (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            if (permissionfineLocation || permissionCoarseLocation) {
                return true;
            }else{
                if (context instanceof AppCompatActivity) {
                    PermissionUtils.requestPermission((AppCompatActivity) context, LOCATION_PERMISSION_REQUEST_CODE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION, true);
                } else if (context instanceof FragmentActivity) {
                    PermissionUtils.requestPermission((FragmentActivity) context, LOCATION_PERMISSION_REQUEST_CODE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION, true);
                }

                return false;
            }
        }catch (Exception e) {
            return false;
        }
    }
}

package com.franciscorp.paymentez.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.activities.DetailActivity;
import com.franciscorp.paymentez.dtos.Place;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.franciscorp.paymentez.activities.DetailActivity.KEY_IMAGE;
import static com.franciscorp.paymentez.activities.DetailActivity.KEY_PLACE;

public class AdapterRecyclerPlaces extends RecyclerView.Adapter<AdapterRecyclerPlaces.ViewHolder> {
    private final GeoDataClient mGeoDataClient;
    private final Context context;
    private final Bitmap bitmapDefault;
    private ArrayList<Place> mDataset;
    private String TAG = "AdapterRecyclerPlaces";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public TextView mTextView;
        public ImageView mImageView;
        public ProgressBar progress;

        public ViewHolder(View view) {
            super(view);
            this.view= view;
            mTextView = view.findViewById(R.id.name);
            mImageView = view.findViewById(R.id.image);
            progress = view.findViewById(R.id.progress);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerPlaces(ArrayList<Place> myDataset, Context context) {
        this.mDataset = myDataset;
        this.context = context;

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(context);

        bitmapDefault = BitmapFactory.decodeResource(context.getResources(),R.drawable.default_image);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AdapterRecyclerPlaces.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler, parent, false);


        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position).getName());

        if(mDataset.get(position).getImage()==null) {
            if (mDataset.get(position).getImageAvailable()) {
                holder.mImageView.setImageBitmap(null);
                getPhotos(mDataset.get(position).getId(), position);
            } else {
                holder.mImageView.setImageBitmap(bitmapDefault);
                holder.progress.setVisibility(View.GONE);
            }
        }else {
            holder.mImageView.setImageBitmap(mDataset.get(position).getImage());
            holder.progress.setVisibility(View.GONE);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                if (mDataset.get(position).getImage()!=null) {
                    mDataset.get(position).getImage().compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    intent.putExtra(KEY_IMAGE, byteArray);
                }
                intent.putExtra(KEY_PLACE,mDataset.get(position));

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                    ActivityOptions options = ActivityOptions
                            .makeSceneTransitionAnimation((Activity) context, holder.mImageView, "image_transition");
                    // start the new activity
                    context.startActivity(intent, options.toBundle());
                }else {
                    context.startActivity(intent);
                }
            }
        });
    }

    private void getPhotos(String placeId, final int position) {

        final Task<PlacePhotoMetadataResponse> photoMetadataResponse = mGeoDataClient.getPlacePhotos(placeId);
        photoMetadataResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoMetadataResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlacePhotoMetadataResponse> task) {
                // Get the list of photos.
                PlacePhotoMetadataResponse photos = task.getResult();
                // Get the PlacePhotoMetadataBuffer (metadata for all of the photos).
                PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                // Get the first photo in the list.
                if (photoMetadataBuffer.getCount()!=0) {
                    PlacePhotoMetadata photoMetadata = photoMetadataBuffer.get(0);
                    // Get the attribution text.
                    CharSequence attribution = photoMetadata.getAttributions();
                    // Get a full-size bitmap for the photo.
                    Task<PlacePhotoResponse> photoResponse = mGeoDataClient.getPhoto(photoMetadata);
                    photoResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoResponse>() {
                        @Override
                        public void onComplete(@NonNull Task<PlacePhotoResponse> task) {
                            try {
                                PlacePhotoResponse photo = task.getResult();
                                Bitmap bitmap = photo.getBitmap();
                                mDataset.get(position).setImage(bitmap);
                                mDataset.get(position).setImageAvailable(true);
                                notifyDataSetChanged();
                            }catch (Exception e){
                                if (e!=null) {
                                    if (e.getMessage()!=null)
                                        Log.e(TAG, e.getMessage());
                                }
                            }
                        }
                    });
                }else{
                    mDataset.get(position).setImageAvailable(false);
                    notifyDataSetChanged();
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mDataset!=null) {
            return mDataset.size();
        }
        return 0;
    }
}
package com.franciscorp.paymentez.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.franciscorp.paymentez.R;
import com.franciscorp.paymentez.dtos.Product;
import com.franciscorp.paymentez.managers.ManagerAnimations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterListProducts extends BaseAdapter {

    private LayoutInflater inflater=null;
    private Context context;
    private ArrayList<Product> data;

    public AdapterListProducts(Context context, ArrayList data) {
        this.context = context;
        this.data=data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        int count=0;
        if (data != null) {
            count = data.size();
        }
        return count;
    }

    @Override
    public Product getItem(int position) {
        return data.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null) {
            vi = inflater.inflate(R.layout.item_list_product, null);
        }

        ImageView imageView =vi.findViewById(R.id.product_image);
        TextView nameView = vi.findViewById(R.id.product_title);
        TextView priceView = vi.findViewById(R.id.product_price);

        Product product =  data.get(position);

        if (!TextUtils.isEmpty(product.getUrlImge())) {
            Picasso.get().load(product.getUrlImge()).placeholder(R.drawable.no_image).into(imageView);
        }else{
            imageView.setImageResource(R.drawable.no_image);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ManagerAnimations.addViewCircle(imageView);
        }else {
            ManagerAnimations.addViewTop(imageView, 400);
        }

        nameView.setText(product.getName());
        priceView.setText(product.getPrice());

        return vi;
    }
}

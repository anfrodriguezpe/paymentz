package com.franciscorp.paymentez.dtos;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;
import java.util.Locale;

public class Place implements Parcelable {

    private String Id;
    private List<Integer> placeTypes;
    private String address;
    private Locale locale;
    private String name;
    private LatLng latLng;
    private LatLngBounds viewport;
    private Uri websiteUri;
    private String phoneNumber;
    private float rating;
    private int priceLevel;
    private String attributions;
    private Bitmap image;
    private Boolean imageAvailable;

    public Place(String id, List<Integer> placeTypes, CharSequence address,
                 Locale locale, CharSequence name, LatLng latLng, LatLngBounds viewport,
                 Uri websiteUri, CharSequence phoneNumber, float rating, int priceLevel, CharSequence attributions) {
        Id = id;
        this.placeTypes = placeTypes;
        if (address != null) {
            this.address = address.toString();
        }
        this.locale = locale;
        if (name != null) {
            this.name = name.toString();
        }
        this.latLng = latLng;
        this.viewport = viewport;
        this.websiteUri = websiteUri;
        if (phoneNumber != null) {
            this.phoneNumber = phoneNumber.toString();
        }
        this.rating = rating;
        this.priceLevel = priceLevel;
        if (attributions != null) {
            this.attributions = attributions.toString();
        }
        this.imageAvailable = true;
    }

    public Place() {
    }

    protected Place(Parcel in) {
        Id = in.readString();
        address = in.readString();
        name = in.readString();
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        viewport = in.readParcelable(LatLngBounds.class.getClassLoader());
        websiteUri = in.readParcelable(Uri.class.getClassLoader());
        phoneNumber = in.readString();
        rating = in.readFloat();
        priceLevel = in.readInt();
        attributions = in.readString();
        imageAvailable = in.readByte() != 0;     //imageAvailable == true if byte != 0

    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public List<Integer> getPlaceTypes() {
        return placeTypes;
    }

    public void setPlaceTypes(List<Integer> placeTypes) {
        this.placeTypes = placeTypes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLngBounds getViewport() {
        return viewport;
    }

    public void setViewport(LatLngBounds viewport) {
        this.viewport = viewport;
    }

    public Uri getWebsiteUri() {
        return websiteUri;
    }

    public void setWebsiteUri(Uri websiteUri) {
        this.websiteUri = websiteUri;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public String getAttributions() {
        return attributions;
    }

    public void setAttributions(String attributions) {
        this.attributions = attributions;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(Id);
        dest.writeString(address);
        dest.writeString(name);
        dest.writeParcelable(latLng, flags);
        dest.writeParcelable(viewport, flags);
        dest.writeParcelable(websiteUri, flags);
        dest.writeString(phoneNumber);
        dest.writeFloat(rating);
        dest.writeInt(priceLevel);
        dest.writeString(attributions);
        dest.writeByte((byte) (imageAvailable ? 1 : 0));     //if imageAvailable == true, byte == 1
    }

    public Boolean getImageAvailable() {
        return imageAvailable;
    }

    public void setImageAvailable(Boolean imageAvailable) {
        this.imageAvailable = imageAvailable;
    }
}

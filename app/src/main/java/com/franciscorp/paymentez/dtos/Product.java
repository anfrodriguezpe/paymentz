package com.franciscorp.paymentez.dtos;

public class Product {

    private String urlImge;
    private String name;
    private String price;

    public Product(String urlImge, String name, String price) {
        this.urlImge = urlImge;
        this.name = name;
        this.price = price;
    }

    public String getUrlImge() {
        return urlImge;
    }

    public void setUrlImge(String urlImge) {
        this.urlImge = urlImge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
